disable_mlock = true
disable_cache = true

storage "mysql" {
 database = "keyDB"
 username = "root"
 password = "kec3is17"
}

listener "tcp" {
 address     = "127.0.0.1:8200"
 tls_disable = "true"
}

api_addr= "http://127.0.0.1:8200"
