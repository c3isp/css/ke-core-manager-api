/**
 * NOTE: This class is auto generated by the swagger code generator program (1.0.11).
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */
package fr.cea.kemanager.restapi;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.cea.kemanager.encrypt.models.BinaryArrayObject;
import fr.cea.kemanager.encrypt.models.EncryptObjectRequest;
import fr.cea.kemanager.key.models.KeyObject;
import fr.cea.kemanager.lib.DataObject;
import fr.cea.kemanager.lib.ObjectParameterRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-03-20T09:57:34.416Z")

@Api(value = "ke-core", description = "the ke-core API")
public interface KeCoreApi {
	
	String FHE_ALGORITHM = "BLACK_LIST_FULL, BLACK_LIST_HIGH, BLACK_LIST_MEDIUM, BLACK_LIST_LOW"; 

    Logger log = LoggerFactory.getLogger(KeCoreApi.class);

    default Optional<ObjectMapper> getObjectMapper() {
        return Optional.empty();
    }

    default Optional<HttpServletRequest> getRequest() {
        return Optional.empty();
    }

    default Optional<String> getAcceptHeader() {
        return getRequest().map(r -> r.getHeader("Accept"));
    }

    @ApiOperation(value = "configuration connection request", nickname = "ping", notes = "Test connexion", response = String.class, tags={ "ping", })
    @ApiResponses(value = {
    @ApiResponse(code = 200, message = "server response", response = String.class),
    @ApiResponse(code = 400, message = "Bad request", response = String.class),
    @ApiResponse(code = 404, message = "Not found", response = String.class) })
    @RequestMapping(value = "/bundle/ping/{userName}", method = RequestMethod.GET)
    ResponseEntity<String> sayPing(@ApiParam(value = "user name to return",required=true) @PathVariable("userName") String userName);
    
    @ApiOperation(value = "decrypt a bundle data for analysis", nickname = "decrypt", 
    		notes = "decrypt a bundle data with a required request Id, DSAId without FHE analysis engine", response = BinaryArrayObject.class, tags={ "data", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Server response", response = BinaryArrayObject.class),
        @ApiResponse(code = 400, message = "Bad request", response = String.class),
        @ApiResponse(code = 404, message = "Not found", response = String.class) })
    @RequestMapping(value = "/ke-core/decrypt/{requestId}/dsa/{DSAId}",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.POST)
    default ResponseEntity<BinaryArrayObject> decrypt(@ApiParam(value = "",required=true) @PathVariable("requestId") String requestId,@ApiParam(value = "",required=true) @PathVariable("DSAId") String dsAId,@ApiParam(value = ""  )  @Valid @RequestBody EncryptObjectRequest encryptObjectRequest) {
        if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
            if (getAcceptHeader().get().contains("application/json")) {
                try {
                    return new ResponseEntity<>(getObjectMapper().get().readValue("{  \"binaryObj\" : \"binaryObj\",  \"requestId\" : \"request_id-abcd-xytzssqd\",  \"otherProperties\" : {    \"key\" : \"otherProperties\"  },  \"objName\" : \"obbj-name.pk\"}", BinaryArrayObject.class), HttpStatus.NOT_IMPLEMENTED);
                } catch (IOException e) {
                    log.error("Couldn't serialize response for content type application/json", e);
                    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default KeCoreApi interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
    
    @ApiOperation(value = "Homomorphic decryption of data using DSA Id and FHE model.", nickname = "decryptFhe", 
			notes = "Data must be the content returned by the Homomorphic encryption API.", response = DataObject.class, tags={ "data", })
	@ApiResponses(value = { 
	    @ApiResponse(code = 200, message = "Server response", response = DataObject.class),
	    @ApiResponse(code = 400, message = "Bad request", response = String.class),
	    @ApiResponse(code = 404, message = "Not found", response = String.class) })
	@RequestMapping(value = "/fhe/decrypt/{RequestId}/dsa/{DSAId}/scheme/{FHEModel}",
	    produces = { "application/json" }, 
	    consumes = { "multipart/form-data" },
	    method = RequestMethod.POST)
	default ResponseEntity<DataObject> decryptFhe(    		
			@ApiParam(value = "",required=true) @PathVariable("RequestId") String requestId,
			@ApiParam(value = "",required=true) @PathVariable("DSAId") String dsAId,
			@ApiParam(value = "",required=true, allowableValues = FHE_ALGORITHM) @PathVariable("FHEModel") String fhEModel, 
			@ApiParam(value = "Ciphertext file") @Valid @RequestPart(value="cipherTextFile") MultipartFile cipherTextFile) {
		if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
	        if (getAcceptHeader().get().contains("application/json")) {
	            try {
	                return null; 
	            } catch (Exception e) {
	                log.error("Couldn't serialize response for content type application/json", e);
	                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	            }
	        }
	    } else {
	        log.warn("ObjectMapper or HttpServletRequest not configured in default DataApi interface so no example is generated");
	    }
	    return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
	}



    @ApiOperation(value = "encrypt a bundle data", nickname = "encrypt", notes = "encrypt bundle data, in function of FHE or Standard technology to encrypt data, the data can be", response = BinaryArrayObject.class, tags={ "data", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Server response contains binary encrypted object", response = BinaryArrayObject.class),
        @ApiResponse(code = 400, message = "Bad request", response = String.class),
        @ApiResponse(code = 404, message = "Not found", response = String.class) })
    @RequestMapping(value = "/ke-core/encrypt/{requestId}/dsa/{DSAId}",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.POST)
    default ResponseEntity<BinaryArrayObject> encrypt(@ApiParam(value = "",required=true) @PathVariable("requestId") String requestId,@ApiParam(value = "",required=true) @PathVariable("DSAId") String dsAId,@ApiParam(value = ""  )  @Valid @RequestBody EncryptObjectRequest encryptObjectRequest) {
        if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
            if (getAcceptHeader().get().contains("application/json")) {
                try {
                    return new ResponseEntity<>(getObjectMapper().get().readValue("{  \"binaryObj\" : \"binaryObj\",  \"requestId\" : \"request_id-abcd-xytzssqd\",  \"otherProperties\" : {    \"key\" : \"otherProperties\"  },  \"objName\" : \"obbj-name.pk\"}", BinaryArrayObject.class), HttpStatus.NOT_IMPLEMENTED);
                } catch (IOException e) {
                    log.error("Couldn't serialize response for content type application/json", e);
                    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default KeCoreApi interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }


    @ApiOperation(value = "encrypt a bundle data with encrypted fields", nickname = "encryptWithTransciphering", notes = "encrypt bundle data, in function of FHE or Standard technology to encrypt data, the data can be", response = BinaryArrayObject.class, tags={ "data", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Server response contains binary encrypted object", response = BinaryArrayObject.class),
        @ApiResponse(code = 400, message = "Bad request", response = String.class),
        @ApiResponse(code = 404, message = "Not found", response = String.class) })
    @RequestMapping(value = "/ke-core/encrypt/dsa/{DSAId}/transciphering",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.POST)
    default ResponseEntity<BinaryArrayObject> encryptWithTransciphering(@ApiParam(value = "" ,required=true )  @Valid @RequestBody ObjectParameterRequest objectTransEncrypting,@ApiParam(value = "",required=true) @PathVariable("DSAId") String dsAId) {
        if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
            if (getAcceptHeader().get().contains("application/json")) {
                try {
                    return new ResponseEntity<>(getObjectMapper().get().readValue("{  \"binaryObj\" : \"binaryObj\",  \"requestId\" : \"request_id-abcd-xytzssqd\",  \"otherProperties\" : {    \"key\" : \"otherProperties\"  },  \"objName\" : \"obbj-name.pk\"}", BinaryArrayObject.class), HttpStatus.NOT_IMPLEMENTED);
                } catch (IOException e) {
                    log.error("Couldn't serialize response for content type application/json", e);
                    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default KeCoreApi interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }


    @ApiOperation(value = "get key from requestId and DSAId with specific FHE algorithm model", nickname = "getKey", notes = "retrieving a set of keys using for FHE Analytic Engine", response = KeyObject.class, responseContainer = "List", tags={ "key", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Server response", response = KeyObject.class, responseContainer = "List"),
        @ApiResponse(code = 400, message = "Bad request", response = String.class),
        @ApiResponse(code = 404, message = "Not found", response = String.class) })
    @RequestMapping(value = "/ke-core/get/evalFHEkey/{RequestId}/dsa/{DSAId}/scheme/{FHEModel}",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    default ResponseEntity<List<KeyObject>> getKey(@ApiParam(value = "",required=true) @PathVariable("RequestId") String requestId,@ApiParam(value = "",required=true) @PathVariable("DSAId") String dsAId,@ApiParam(value = "",required=true, allowableValues = "\"BLACK_LIST\", \"PATTERN_MATCHING\"") @PathVariable("FHEModel") String fhEModel) {
        if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
            if (getAcceptHeader().get().contains("application/json")) {
                try {
                    return new ResponseEntity<>(getObjectMapper().get().readValue("[ {  \"requestId\" : \"request_id-abcd-xytzssqd\",  \"checksum\" : \"checksum HASH for all the keys\",  \"keyInfo\" : \"public key\",  \"key\" : \"\"}, {  \"requestId\" : \"request_id-abcd-xytzssqd\",  \"checksum\" : \"checksum HASH for all the keys\",  \"keyInfo\" : \"public key\",  \"key\" : \"\"} ]", List.class), HttpStatus.NOT_IMPLEMENTED);
                } catch (IOException e) {
                    log.error("Couldn't serialize response for content type application/json", e);
                    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default KeCoreApi interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

}
