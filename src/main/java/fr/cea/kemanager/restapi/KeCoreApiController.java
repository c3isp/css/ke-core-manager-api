package fr.cea.kemanager.restapi;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.cea.kemanager.components.FheKeyController;
import fr.cea.kemanager.dpos.api.DataApi;
import fr.cea.kemanager.encrypt.models.BinaryArrayObject;
import fr.cea.kemanager.encrypt.models.EncryptObjectRequest;
import fr.cea.kemanager.fhe.api.KeyApi;
import fr.cea.kemanager.helper.BashTools;
import fr.cea.kemanager.helper.Constants;
import fr.cea.kemanager.helper.ToolsFolder;
import fr.cea.kemanager.key.models.KeyObject;
import fr.cea.kemanager.lib.DataObject;
import fr.cea.kemanager.lib.ObjectParameterRequest;
import fr.cea.kemanager.lib.client.ApiClient;
import fr.cea.kemanager.lib.client.ApiException;
import fr.cea.kemanager.models.FileOperations;
import fr.cea.kemanager.models.KeyTransfer;
import io.swagger.annotations.ApiParam;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-03-20T09:57:34.416Z")

@RestController
@RequestMapping("/v1")
public class KeCoreApiController implements KeCoreApi {

	private final ObjectMapper objectMapper;
	private final int ANALYSIS_TIME_OUT = 300;

	private final HttpServletRequest request;

	@Value("${rest.endpoint.url.callGetDPOSEncryption}")
	private String callGetDPOSEncryptionEndpoint;
	@Value("${rest.endpoint.url.callGetFHEKeys}")
	private String callGetFHEKeysEndpoint;


	@Value("${security.user.dposHeader}")
	private String dposHeaderAuth;
	@Value("${security.user.dposUsrPassword}")
	private String dposUsrPassword;
	@Value("${security.user.fheHeader}")
	private String fheHeaderAuth;
	@Value("${security.user.fheUsrPassword}")
	private String fheUsrPassword;

	@Autowired
	public KeCoreApiController(ObjectMapper objectMapper, HttpServletRequest request) {
		this.objectMapper = objectMapper;
		this.request = request;
	}
	
	@Autowired
	private FheKeyController fheKeyController;

	@Override
	public Optional<ObjectMapper> getObjectMapper() {
		return Optional.ofNullable(objectMapper);
	}

	@Override
	public Optional<HttpServletRequest> getRequest() {
		return Optional.ofNullable(request);
	}
	
	@ExceptionHandler
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public void handle(Exception e) {
	    log.warn("[KE-CORE-API] Returning HTTP 400 Bad Request", e);
	}
	
//	@ExceptionHandler
//	@ResponseStatus(HttpStatus.UNAUTHORIZED)
//	public void handleUnauthorized(Exception e) {
//	    log.warn("[KE-CORE-API] Returning HTTP 401 Unauthorized Request", e);
//	}

	@Override
	public ResponseEntity<BinaryArrayObject> decrypt(
			@ApiParam(value = "",required=true) @PathVariable("requestId") String requestId,
			@ApiParam(value = "",required=true) @PathVariable("DSAId") String dsAId,
			@ApiParam(value = ""  )  @Valid @RequestBody EncryptObjectRequest encryptObjectRequest) {
		if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
			if (getAcceptHeader().get().contains("application/json")) {
				try {
					log.info(
							String.format("[KE-CORE-API] ResponseEntity<BinaryArrayObject> DECRYPT method start working with ReqId = %s and DSAID = %s", 
									requestId, dsAId));
					ApiClient apiClient = new ApiClient().setBasePath(callGetDPOSEncryptionEndpoint);
					apiClient.addDefaultHeader(dposHeaderAuth, dposUsrPassword);
					//DataApi DataApi = new DataApi(apiClient);
					DataObject dataObject = new DataObject();
					dataObject = dataObject.dataContent(
									encryptObjectRequest.getBinaryObj())
								.dataName(requestId + "_decrypted_"+dsAId)
								.checksum(Integer.toString(hashCode()))
								.urlToBack("http://url_to_back_here");

					String localVarPath = "/dpos/decrypt/{RequestId}/dsa/{DSAId}"
				            .replaceAll("\\{" + "RequestId" + "\\}", apiClient.escapeString(requestId.toString()))
				            .replaceAll("\\{" + "DSAId" + "\\}", apiClient.escapeString(dsAId.toString()));
					String fullPath = apiClient.getBasePath() + localVarPath; 
					log.info("[KE-CORE-API] Sending request to DPOS-Encryption : " + fullPath + " with length data = " + dataObject.getDataContent().length());
					
					//RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());
					RestTemplate restTemplate = new RestTemplate();
					DataObject decryptedContent = null;
					try {
						decryptedContent = restTemplate.postForObject( fullPath, dataObject, DataObject.class);	
					} catch (HttpClientErrorException e) {
						System.out.println("[KE-CORE-API] HttpClientErrorException decrypt to KE-CORE-API \n " + e.toString());
					} catch (RestClientException e) {
						System.out.println("[KE-CORE-API] Error executing request decrypt to KE-CORE-API \n " + e.toString());
						System.out.println("===============================================");
						System.out.println("[KE-CORE-API] sending request via : " + fullPath);
						System.out.println("===============================================");
					}
//					
					//DataObject decryptedContent = DataApi.decrypt(dataObject, requestId, dsAId);
					
					if(decryptedContent == null ) throw new Exception("Error at server KE Manager ! Cannot decrypt data");
					BinaryArrayObject decrypted = new BinaryArrayObject()
								.binaryObj(decryptedContent.getDataContent())
								.objName(requestId + "_decrypted_"+dsAId)							
								.requestId(requestId);
					
					log.info(
							String.format("[KE-CORE-API] BinaryArrayObject DECRYPT method OK back to request with length %d ", decrypted.getBinaryObj().length()));
					return new ResponseEntity<BinaryArrayObject>(decrypted, HttpStatus.OK);
				} catch (fr.cea.kemanager.lib.client.ApiException e) {
					log.error("[KE-CORE-API] Couldn't serialize response for content type application/json", e);
					return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
				} catch (Exception e) {
					e.printStackTrace();
					return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
				}
			}
		} else {
			log.warn("ObjectMapper or HttpServletRequest not configured in default KeCoreApi interface so no example is generated");
		}
		return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
	}
	
	@Override
    public ResponseEntity<DataObject> decryptFhe(    		
    		@ApiParam(value = "",required=true) @PathVariable("RequestId") String requestId,
    		@ApiParam(value = "",required=true) @PathVariable("DSAId") String dsAId,
    		@ApiParam(value = "",required=true, allowableValues = FHE_ALGORITHM) @PathVariable("FHEModel") String fhEModel, 
    		@ApiParam(value = "Ciphertext file") @RequestPart(value="cipherTextFile") MultipartFile cipherTextFile) {
    	if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
            if (getAcceptHeader().get().contains("application/json")) {
                try {                	                
                	log.info("[KE-CORE-API] decrypt method start working");
                	/**
                	 * Checking existing key ?
                	 * */
                	
                	String fileName = cipherTextFile.getName(); 
					byte[] content = cipherTextFile.getBytes();
					File ctFile = ToolsFolder.createTempFile("m_0", ".ct", content);
					log.info("[KE-CORE-API] the temp file was created at " + ctFile.getAbsolutePath() + " at parent = " + ctFile.getParent());
					DataObject reServerResponse = decryptCipherText(requestId, dsAId, fhEModel, fileName, ctFile); 
					return new ResponseEntity<DataObject>(reServerResponse, HttpStatus.OK);           	
                } catch (NumberFormatException e) {
                    log.error("NumberFormatException Couldn't serialize response for content type application/json", e);
                    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                }  catch (IOException  e) {
                  log.error("IOException Couldn't serialize response for content type application/json", e);
                  return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default DataApi interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
	
	private DataObject decryptCipherText(
			String requestId, String dsAId, String fhEModel, String fileName, File ctFile)
			throws IOException {
		
		fheKeyController.init(requestId, dsAId, fhEModel);                	
		boolean isCreated = fheKeyController.CreatingFHEKeyIfNeed();
		if(isCreated)
		{
			/**
			 * Existed keys, process to analyze now
			 * */
			KeyTransfer lstKeys = fheKeyController.getSkKeyForDecryption();
			log.info("get SK keys from KE-CORE-API with #keys = " + lstKeys.getLstKeys().size());
			assert(lstKeys.getLstKeys() != null && lstKeys.getLstKeys().size() != 0);	
			
			byte[] secretKey = fheKeyController.getFheSecretKey(lstKeys.getLstKeys());
			byte[] param = fheKeyController.getHEParameter(lstKeys.getLstKeys());

			assert(secretKey != null);
			String baseDir = String.format(Constants.FheAlgo.BLACKLIST.ANALYSISDATA_DIR, 
					dsAId, fhEModel + File.separator + requestId);
			
			log.info("Create folder Path " + baseDir);
			ToolsFolder.CreateAllFolderNames(baseDir);
			
			log.info("configuration folder exec : " + Constants.FheScripts.FHE_CONFIG_EXEC + " " + baseDir );
			BashTools.Exec(true, Constants.FheScripts.FHE_CONFIG_EXEC, baseDir, fhEModel);
			
			FileOperations.PrintData(baseDir + File.separator + Constants.FheKeys.FHE_SK_FILE, secretKey);
			FileOperations.PrintData(baseDir + File.separator + Constants.FheKeys.FHE_PARAM_FILE, param);
			
			
			log.info(String.format("Exec %s %s with FolderOffileName = %s", 
					baseDir + File.separator + Constants.FheScripts.FHE_DECRYPT_IP_NAME, 
					ctFile.getParent(), 
					fileName));
			
			String result = BashTools.ExecDecryptFhe(
					baseDir + File.separator + Constants.FheScripts.FHE_DECRYPT_IP_NAME, 
					ctFile.getParent());    		
			log.info("result = " + result);
			DataObject reServerResponse = new DataObject()
					.dataContent(result)
					.dataName("decrypted_"+ dsAId + "_" + fhEModel)
					.checksum("checsum")
					.urlToBack("url_back");
			
			return reServerResponse;
		}
		return new DataObject();
	}

	@Override
	public ResponseEntity<BinaryArrayObject> encrypt(
			@ApiParam(value = "",required=true) @PathVariable("requestId") String requestId,
			@ApiParam(value = "",required=true) @PathVariable("DSAId") String dsAId,
			@ApiParam(value = ""  )  @Valid @RequestBody EncryptObjectRequest encryptObjectRequest) {
		
		if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
			if (getAcceptHeader().get().contains("application/json")) {
				try {
					return __encryptDataObject(requestId, dsAId, encryptObjectRequest);
				} catch (fr.cea.kemanager.lib.client.ApiException e) {
					log.error("[KE-CORE-API] encrypt | Couldn't call APIException ", e);
					return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
				} catch (Exception e) {
					log.error("[KE-CORE-API] encrypt | Couldn't serialize response for content type application/json", e);
					return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
				}
			}
		} else {
			log.warn("[KE-CORE-API] ENCRYPT | ObjectMapper or HttpServletRequest not configured in default KeCoreApi interface so no example is generated");
		}
		return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
	}


	@Override
	public ResponseEntity<BinaryArrayObject> encryptWithTransciphering(
			@ApiParam(value = "" ,required=true )  @Valid @RequestBody ObjectParameterRequest lstParameters,			
			@ApiParam(value = "",required=true) @PathVariable("DSAId") String dsAId) {
		
		if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
			System.out.println("[KE-CORE-API] Receiving request encryptWithTransciphering for " + lstParameters.toString() +" \n With DSA_ID = "+ dsAId);
			if (getAcceptHeader().get().contains("application/json")) {
				try {
					String contentFile = lstParameters.getParamProperties().get(Constants.DefaultParam.CONTENT_FILE_PARAM);
					String encryptedField = lstParameters.getParamProperties().get(Constants.DefaultParam.ENCRYPTED_FIELD_PARAM);
					String requestId = lstParameters.getId();
					System.out.println("[KE-CORE-API] Processing request ID = " + requestId);
					if( !(contentFile != null && encryptedField != null && requestId != null 
					&& contentFile.length() > 0 && encryptedField.length() > 0 && requestId.length() > 0))
						throw new ExceptionInInitializerError(
									"Please verify the variable Content data, field to encrypt and request id");
						
					return __encryptDataObject(requestId, dsAId, contentFile, encryptedField);
				} catch (fr.cea.kemanager.lib.client.ApiException e) {
					log.error("[KE-CORE-API] encrypt | Couldn't call APIException ", e);
					return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
				} catch (Exception e) {
					log.error("[KE-CORE-API] encrypt | Couldn't serialize response for content type application/json", e);
					return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
				}
			}
		} else {
			log.warn("[KE-CORE-API] ENCRYPT | ObjectMapper or HttpServletRequest not configured in default KeCoreApi interface so no example is generated");
		}
		return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
		
	}
	
	@Override
	public ResponseEntity<List<KeyObject>> getKey(
			@ApiParam(value = "",required=true) @PathVariable("RequestId") String requestId,
			@ApiParam(value = "",required=true) @PathVariable("DSAId") String dsAId,
			@ApiParam(value = "",required=true, allowableValues = "BLACK_LIST, PATTERN_MATCHING") @PathVariable("FHEModel") String fhEModel) {
		if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
			if (getAcceptHeader().get().contains("application/json")) {
				log.info("ResponseEntity<List<KeyObject>> getKey method start working");
				ApiClient apiClient = new ApiClient().setBasePath(callGetFHEKeysEndpoint);
				apiClient.addDefaultHeader(fheHeaderAuth, fheUsrPassword);
				KeyApi KeyApi = new KeyApi(apiClient);
				try {
					return new ResponseEntity<List<KeyObject>>(
							KeyApi.getKey("FHE_ALL", requestId, dsAId, fhEModel), HttpStatus.OK);
				} catch (ApiException e) {
					e.printStackTrace();
				}
			}
		} else {
			log.warn("ObjectMapper or HttpServletRequest not configured in default KeCoreApi interface so no example is generated");
		}
		return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
	}
	

	private ResponseEntity<BinaryArrayObject> __encryptDataObject(String requestId, String dsAId,
			EncryptObjectRequest encryptObjectRequest) throws ApiException {
		return __encryptDataObject(requestId, dsAId, encryptObjectRequest.getBinaryObj(), "");
	}
	
	private ResponseEntity<BinaryArrayObject> __encryptDataObject(String requestId, String dsAId,
			String encryptObjectRequest, String transEncryptedField) throws ApiException {
		log.info("[KE-CORE-API] ENCRYPT method start working with request " + requestId);
		
		ApiClient apiClient = new ApiClient().setBasePath(callGetDPOSEncryptionEndpoint);
		apiClient.addDefaultHeader(dposHeaderAuth, dposUsrPassword);
		DataApi dataApi = new DataApi(apiClient);					
		DataObject dataObject = new DataObject()
									.dataContent(encryptObjectRequest)
									.dataName(requestId + "_encrypted_"+dsAId); 
		dataObject.setChecksum(""+ Objects.hash(dataObject.getDataContent()));
		dataObject.urlToBack("https:///ssss")
				.putOtherPropertiesItem(Constants.DefaultParam.ENCRYPTED_FIELD_PARAM, 
										transEncryptedField);
		
		log.info("[KE-CORE-API] ENCRYPT | Sending with request " + requestId + " dsAID = " + dsAId + " and length "+ encryptObjectRequest.length());					
		DataObject dataEncryptedObject = dataApi.encrypt(dataObject, requestId, dsAId); 							
		log.info("[KE-CORE-API] ENCRYPT | encryption OK with dataContent Length = " + dataEncryptedObject.getDataContent().length() );		
		BinaryArrayObject encrypted = new BinaryArrayObject()
				.binaryObj(dataEncryptedObject.getDataContent())
				.objName(requestId + "_encrypted_"+dsAId)
				.requestId(requestId)
				.otherProperties(dataEncryptedObject.getOtherProperties());
		
		log.info("[KE-CORE-API] ENCRYPT | Sucesfully Encrypted ");
		return new ResponseEntity<BinaryArrayObject>(encrypted, HttpStatus.OK);
	}
	
	private ClientHttpRequestFactory getClientHttpRequestFactory() {
	    
	    HttpComponentsClientHttpRequestFactory clientHttpRequestFactory
	      = new HttpComponentsClientHttpRequestFactory();
	    clientHttpRequestFactory.setConnectTimeout(ANALYSIS_TIME_OUT*1000);
	    clientHttpRequestFactory.setReadTimeout(ANALYSIS_TIME_OUT*1000);
	    return clientHttpRequestFactory;
	}
	
	@Override
	public ResponseEntity<String> sayPing(@ApiParam(value = "user name to return",required=true) @PathVariable("userName") String userName) {
		String resp = "say ping to : " + userName + " with parameter callGetDPOSEncryptionEndpoint loaded from config-server : " + callGetDPOSEncryptionEndpoint;
		return new ResponseEntity<String>(resp, HttpStatus.OK);		
	}
	
}
