package fr.cea.kemanager.helper;

import java.io.File;

public class Constants {
	private static final String USR_BASE 				= "/opt";	
	public static final String FHE_REPOSITORY_DIR 		= USR_BASE + File.separator + "FHE_C3ISP" + File.separator + "repository";
	public static final String FHE_ANALYSISDATA_DIR		= USR_BASE + File.separator + "FHE_C3ISP" + File.separator + "analyse";
	
	public static class FheScripts
	{		
		public static final String FHE_CONFIG_FOLDER_EXEC 	= "configuration.sh";
		public static final String FHE_DECRYPT_IP_NAME		= "03-decrypt.sh";					
		public static final String FHE_CONFIG_EXEC = FHE_REPOSITORY_DIR + File.separator + FHE_CONFIG_FOLDER_EXEC;				
	}		
	
	public static class FheAlgo
	{
		public static final String BLACKLIST_CIRCUIT 	= "membership.blif";
		public static final String BLACKLIST_CIRCUIT_OPT = "membership-opt.blif";				
		
		public static class BLACKLIST
		{
			public static final String ANALYSISDATA_DIR = FHE_ANALYSISDATA_DIR + File.separator + "%s" + File.separator + "%s" /*analysis model*/ + File.separator;
			public static final String IP_FOLDER_FORMAT = "IP_%d";
		}
	}
	
	public class FheKeys
	{
		public static final String FHE_PATH_VAULT = "secret/";
		public static final String FHE_EVAL_FILE = "fhe_key.evk";
		public static final String FHE_SK_FILE 	= "fhe_key.sk";
		public static final String FHE_PK_FILE 	= "fhe_key.pk";
		public static final String FHE_PARAM_FILE = "fhe_params.xml";
	}
	
	public class FheConfigKeys
	{
		public static final String FHE_EVAL_SUFFIX 	= "evk";
		public static final String FHE_SK_SUFFIX 	= "sk";
		public static final String FHE_PK_SUFFIX 	= "pk";
		public static final String FHE_PARAM_SUFFIX = "xml";
		public static final String FHE_VERSION_SUFFIX = "ver";
		public static final String FHE_VERSION_VALUE = "value";
	}
	
	public class DefaultParam
	{
		public static final String REQUEST_ID_PARAM 	= "requestID";
		public static final String PAYLOAD_FORMAT_PARAM = "payloadFormat";
		public static final String CONTENT_FILE_PARAM 	= "fileContent";
		public static final String ENCRYPTED_FIELD_PARAM = "encryptedField";
	}
	
	public class EventHandleParam
	{		
		public static final String METADATA_FILE_PARAM 	= "metadataFile";
		public static final String CTI_FILE_PARAM 		= "ctiFile";
		public static final String DSA_ID_PARAM 		= "dsaId";
		public static final String DPOS_ID_PARAM 		= "dposId";		
	}
	
	public class BundleResponseParam
	{				
		public static final String FILE_NAME_PARAM 		= "fileName";
		public static final String STATUS_RESULT_PARAM 	= "result";	
	}
	
	public class C3ispComponents
	{
		public static final String ISI_NODE 			= "isic3isp";
		public static final String CSS_NODE	 			= "cssc3isp";		
		public static final String ISI_BM_COMPONENT 	= "isibmcom";
		public static final String CSS_DPOS_COMPONENT	= "cssdposcom";
	}
	
	public class BundleManagerSuffixFile
	{
		public static final String METADATA_SUFFIX 	= ".head";
		public static final String CTI_SUFFIX 		= ".payload";
		public static final String DSA_SUFFIX 		= ".dsa";
		public static final String HASH_SUFFIX 		= ".sign";
	}
}
