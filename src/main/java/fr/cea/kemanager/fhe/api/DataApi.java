
package fr.cea.kemanager.fhe.api;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.reflect.TypeToken;

import fr.cea.kemanager.lib.DataObject;
import fr.cea.kemanager.lib.Pair;
import fr.cea.kemanager.lib.client.ApiCallback;
import fr.cea.kemanager.lib.client.ApiClient;
import fr.cea.kemanager.lib.client.ApiException;
import fr.cea.kemanager.lib.client.ApiResponse;
import fr.cea.kemanager.lib.client.Configuration;
import fr.cea.kemanager.lib.client.ProgressRequestBody;
import fr.cea.kemanager.lib.client.ProgressResponseBody;

public class DataApi {
    private ApiClient apiClient;

    public DataApi() {
        this(Configuration.getDefaultApiClient());
    }

    public DataApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Build call for decrypt
     * @param fileData The CTI content to upload. (required)
     * @param requestId  (required)
     * @param dsAId  (required)
     * @param fhEModel  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call decryptCall(DataObject fileData, String requestId, String dsAId, String fhEModel, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = fileData;

        // create path and map variables
        String localVarPath = "/fhe/decrypt/{RequestId}/dsa/{DSAId}/scheme/{FHEModel}"
            .replaceAll("\\{" + "RequestId" + "\\}", apiClient.escapeString(requestId.toString()))
            .replaceAll("\\{" + "DSAId" + "\\}", apiClient.escapeString(dsAId.toString()))
            .replaceAll("\\{" + "FHEModel" + "\\}", apiClient.escapeString(fhEModel.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call decryptValidateBeforeCall(DataObject fileData, String requestId, String dsAId, String fhEModel, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'fileData' is set
        if (fileData == null) {
            throw new ApiException("Missing the required parameter 'fileData' when calling decrypt(Async)");
        }
        
        // verify the required parameter 'requestId' is set
        if (requestId == null) {
            throw new ApiException("Missing the required parameter 'requestId' when calling decrypt(Async)");
        }
        
        // verify the required parameter 'dsAId' is set
        if (dsAId == null) {
            throw new ApiException("Missing the required parameter 'dsAId' when calling decrypt(Async)");
        }
        
        // verify the required parameter 'fhEModel' is set
        if (fhEModel == null) {
            throw new ApiException("Missing the required parameter 'fhEModel' when calling decrypt(Async)");
        }
        

        com.squareup.okhttp.Call call = decryptCall(fileData, requestId, dsAId, fhEModel, progressListener, progressRequestListener);
        return call;

    }

    /**
     * decrypt a bundle data with a required request Id, DSAId, and other security parameters
     * store Payload With Metadata, this is used when someone calls the Create CTI from ISI API
     * @param fileData The CTI content to upload. (required)
     * @param requestId  (required)
     * @param dsAId  (required)
     * @param fhEModel  (required)
     * @return DataObject
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public DataObject decrypt(DataObject fileData, String requestId, String dsAId, String fhEModel) throws ApiException {
        ApiResponse<DataObject> resp = decryptWithHttpInfo(fileData, requestId, dsAId, fhEModel);
        return resp.getData();
    }

    /**
     * decrypt a bundle data with a required request Id, DSAId, and other security parameters
     * store Payload With Metadata, this is used when someone calls the Create CTI from ISI API
     * @param fileData The CTI content to upload. (required)
     * @param requestId  (required)
     * @param dsAId  (required)
     * @param fhEModel  (required)
     * @return ApiResponse&lt;DataObject&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<DataObject> decryptWithHttpInfo(DataObject fileData, String requestId, String dsAId, String fhEModel) throws ApiException {
        com.squareup.okhttp.Call call = decryptValidateBeforeCall(fileData, requestId, dsAId, fhEModel, null, null);
        Type localVarReturnType = new TypeToken<DataObject>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * decrypt a bundle data with a required request Id, DSAId, and other security parameters (asynchronously)
     * store Payload With Metadata, this is used when someone calls the Create CTI from ISI API
     * @param fileData The CTI content to upload. (required)
     * @param requestId  (required)
     * @param dsAId  (required)
     * @param fhEModel  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call decryptAsync(DataObject fileData, String requestId, String dsAId, String fhEModel, final ApiCallback<DataObject> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = decryptValidateBeforeCall(fileData, requestId, dsAId, fhEModel, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<DataObject>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for encrypt
     * @param fileData The CTI content to upload. (required)
     * @param requestId  (required)
     * @param dsAId  (required)
     * @param fhEModel  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call encryptCall(DataObject fileData, String requestId, String dsAId, String fhEModel, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = fileData;

        // create path and map variables
        String localVarPath = "/fhe/encrypt/{RequestId}/dsa/{DSAId}/scheme/{FHEModel}"
            .replaceAll("\\{" + "RequestId" + "\\}", apiClient.escapeString(requestId.toString()))
            .replaceAll("\\{" + "DSAId" + "\\}", apiClient.escapeString(dsAId.toString()))
            .replaceAll("\\{" + "FHEModel" + "\\}", apiClient.escapeString(fhEModel.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call encryptValidateBeforeCall(DataObject fileData, String requestId, String dsAId, String fhEModel, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'fileData' is set
        if (fileData == null) {
            throw new ApiException("Missing the required parameter 'fileData' when calling encrypt(Async)");
        }
        
        // verify the required parameter 'requestId' is set
        if (requestId == null) {
            throw new ApiException("Missing the required parameter 'requestId' when calling encrypt(Async)");
        }
        
        // verify the required parameter 'dsAId' is set
        if (dsAId == null) {
            throw new ApiException("Missing the required parameter 'dsAId' when calling encrypt(Async)");
        }
        
        // verify the required parameter 'fhEModel' is set
        if (fhEModel == null) {
            throw new ApiException("Missing the required parameter 'fhEModel' when calling encrypt(Async)");
        }
        

        com.squareup.okhttp.Call call = encryptCall(fileData, requestId, dsAId, fhEModel, progressListener, progressRequestListener);
        return call;

    }

    /**
     * create a bundle data with a required request Id, DSAId, and other security parameters
     * store Payload With Metadata, this is used when someone calls the Create CTI from ISI API
     * @param fileData The CTI content to upload. (required)
     * @param requestId  (required)
     * @param dsAId  (required)
     * @param fhEModel  (required)
     * @return DataObject
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public DataObject encrypt(DataObject fileData, String requestId, String dsAId, String fhEModel) throws ApiException {
        ApiResponse<DataObject> resp = encryptWithHttpInfo(fileData, requestId, dsAId, fhEModel);
        return resp.getData();
    }

    /**
     * create a bundle data with a required request Id, DSAId, and other security parameters
     * store Payload With Metadata, this is used when someone calls the Create CTI from ISI API
     * @param fileData The CTI content to upload. (required)
     * @param requestId  (required)
     * @param dsAId  (required)
     * @param fhEModel  (required)
     * @return ApiResponse&lt;DataObject&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<DataObject> encryptWithHttpInfo(DataObject fileData, String requestId, String dsAId, String fhEModel) throws ApiException {
        com.squareup.okhttp.Call call = encryptValidateBeforeCall(fileData, requestId, dsAId, fhEModel, null, null);
        Type localVarReturnType = new TypeToken<DataObject>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * create a bundle data with a required request Id, DSAId, and other security parameters (asynchronously)
     * store Payload With Metadata, this is used when someone calls the Create CTI from ISI API
     * @param fileData The CTI content to upload. (required)
     * @param requestId  (required)
     * @param dsAId  (required)
     * @param fhEModel  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call encryptAsync(DataObject fileData, String requestId, String dsAId, String fhEModel, final ApiCallback<DataObject> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = encryptValidateBeforeCall(fileData, requestId, dsAId, fhEModel, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<DataObject>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
