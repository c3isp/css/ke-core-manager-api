

package fr.cea.kemanager.dpos.api;


import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.reflect.TypeToken;

import fr.cea.kemanager.key.models.KeyObject;
import fr.cea.kemanager.lib.Pair;
import fr.cea.kemanager.lib.client.ApiCallback;
import fr.cea.kemanager.lib.client.ApiClient;
import fr.cea.kemanager.lib.client.ApiException;
import fr.cea.kemanager.lib.client.ApiResponse;
import fr.cea.kemanager.lib.client.Configuration;
import fr.cea.kemanager.lib.client.ProgressRequestBody;
import fr.cea.kemanager.lib.client.ProgressResponseBody;

public class KeyApi {
    private ApiClient apiClient;

    public KeyApi() {
        this(Configuration.getDefaultApiClient());
    }

    public KeyApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Build call for createKey
     * @param requestId  (required)
     * @param dsAId  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call createKeyCall(String requestId, String dsAId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/dpos/key/create/{requestId}/dsa/{DSAId}"
            .replaceAll("\\{" + "requestId" + "\\}", apiClient.escapeString(requestId.toString()))
            .replaceAll("\\{" + "DSAId" + "\\}", apiClient.escapeString(dsAId.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call createKeyValidateBeforeCall(String requestId, String dsAId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'requestId' is set
        if (requestId == null) {
            throw new ApiException("Missing the required parameter 'requestId' when calling createKey(Async)");
        }
        
        // verify the required parameter 'dsAId' is set
        if (dsAId == null) {
            throw new ApiException("Missing the required parameter 'dsAId' when calling createKey(Async)");
        }
        

        com.squareup.okhttp.Call call = createKeyCall(requestId, dsAId, progressListener, progressRequestListener);
        return call;

    }

    /**
     * create key from requestId
     * in function of DPOS or FHE component, we start to create a corresponding set of keys
     * @param requestId  (required)
     * @param dsAId  (required)
     * @return String
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public String createKey(String requestId, String dsAId) throws ApiException {
        ApiResponse<String> resp = createKeyWithHttpInfo(requestId, dsAId);
        return resp.getData();
    }

    /**
     * create key from requestId
     * in function of DPOS or FHE component, we start to create a corresponding set of keys
     * @param requestId  (required)
     * @param dsAId  (required)
     * @return ApiResponse&lt;String&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<String> createKeyWithHttpInfo(String requestId, String dsAId) throws ApiException {
        com.squareup.okhttp.Call call = createKeyValidateBeforeCall(requestId, dsAId, null, null);
        Type localVarReturnType = new TypeToken<String>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * create key from requestId (asynchronously)
     * in function of DPOS or FHE component, we start to create a corresponding set of keys
     * @param requestId  (required)
     * @param dsAId  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call createKeyAsync(String requestId, String dsAId, final ApiCallback<String> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = createKeyValidateBeforeCall(requestId, dsAId, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<String>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getKey
     * @param requestId  (required)
     * @param dsAId  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getKeyCall(String requestId, String dsAId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/dpos/key/get/{requestId}/dsa/{DSAId}"
            .replaceAll("\\{" + "requestId" + "\\}", apiClient.escapeString(requestId.toString()))
            .replaceAll("\\{" + "DSAId" + "\\}", apiClient.escapeString(dsAId.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getKeyValidateBeforeCall(String requestId, String dsAId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'requestId' is set
        if (requestId == null) {
            throw new ApiException("Missing the required parameter 'requestId' when calling getKey(Async)");
        }
        
        // verify the required parameter 'dsAId' is set
        if (dsAId == null) {
            throw new ApiException("Missing the required parameter 'dsAId' when calling getKey(Async)");
        }
        

        com.squareup.okhttp.Call call = getKeyCall(requestId, dsAId, progressListener, progressRequestListener);
        return call;

    }

    /**
     * get key from requestId
     * in function of DPOS or FHE analytics, we provide a corresponding set of keys
     * @param requestId  (required)
     * @param dsAId  (required)
     * @return List&lt;KeyObject&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public List<KeyObject> getKey(String requestId, String dsAId) throws ApiException {
        ApiResponse<List<KeyObject>> resp = getKeyWithHttpInfo(requestId, dsAId);
        return resp.getData();
    }

    /**
     * get key from requestId
     * in function of DPOS or FHE analytics, we provide a corresponding set of keys
     * @param requestId  (required)
     * @param dsAId  (required)
     * @return ApiResponse&lt;List&lt;KeyObject&gt;&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<List<KeyObject>> getKeyWithHttpInfo(String requestId, String dsAId) throws ApiException {
        com.squareup.okhttp.Call call = getKeyValidateBeforeCall(requestId, dsAId, null, null);
        Type localVarReturnType = new TypeToken<List<KeyObject>>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * get key from requestId (asynchronously)
     * in function of DPOS or FHE analytics, we provide a corresponding set of keys
     * @param requestId  (required)
     * @param dsAId  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getKeyAsync(String requestId, String dsAId, final ApiCallback<List<KeyObject>> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = getKeyValidateBeforeCall(requestId, dsAId, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<List<KeyObject>>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
