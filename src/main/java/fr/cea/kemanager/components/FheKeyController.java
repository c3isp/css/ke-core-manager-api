package fr.cea.kemanager.components;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import fr.cea.kemanager.helper.Constants;
import fr.cea.kemanager.helper.ToolsFolder;
import fr.cea.kemanager.models.*;
import fr.cea.kemanager.models.KeyObject.KeyInfoEnum;

@Component
public class FheKeyController {

	Logger log = LoggerFactory.getLogger(FheKeyController.class); 
	private final String logStatus = "[FHE-ENCRYPT-MANAGER] ";	

	private String baseUrl; 
	private String requestId; 
	private String dsAId;
	private String fhEModel;
	private RestTemplate m_restTemplate;
	
	private String baseCheckKeyUrl;
	private String baseGetKeyUrl;
	private String baseGetSkKeyUrl;
	private String baseCreateKeyUrl;
		
	private String checkKeyUrl 	= "";
	private String getKeyUrl 	= "";
	private String getSkKeyUrl 	= "";
	private String createKeyUrl = "";
	
	@Autowired
	public FheKeyController(			
		    @Value("${rest.endpoint.url.callFheKeyCoreBase}") 	String baseUrl, 
			@Value("${security.user.name}") 					String restUser,
			@Value("${security.user.password}") 				String restPassword, 
			@Value("${rest.endpoint.url.callFheCheckKeyUrl}")	String baseCheckKeyUrl,
			@Value("${rest.endpoint.url.callFheBaseGetKeyUrl}")	String baseGetKeyUrl,
			@Value("${rest.endpoint.url.callFheBaseGetSkKeyUrl}") 	String baseGetSkKeyUrl,
			@Value("${rest.endpoint.url.callFheBaseCreateKeyUrl}") String baseCreateKeyUrl) {
		super();
		this.baseUrl 			= baseUrl;
		this.baseCheckKeyUrl 	= baseCheckKeyUrl; 
		this.baseGetKeyUrl 		= baseGetKeyUrl; 
		this.baseGetSkKeyUrl	= baseGetSkKeyUrl; 
		this.baseCreateKeyUrl 	= baseCreateKeyUrl;
		m_restTemplate = new RestTemplate();
		m_restTemplate.getInterceptors().add(
				  new BasicAuthorizationInterceptor(restUser, restPassword));
	}

	
	public void init (String requestId, String dsAId, String fhEModel) {				
		this.requestId = requestId;
		this.dsAId = dsAId;
		this.fhEModel = fhEModel;
		
		checkKeyUrl = baseCheckKeyUrl.replaceAll("\\{" + "RequestId" + "\\}", requestId)
								.replaceAll("\\{" + "DSAId" + "\\}", dsAId)
								.replaceAll("\\{" + "FHEModel" + "\\}", fhEModel);
			  
		getKeyUrl = baseGetKeyUrl
				.replaceAll("\\{" + "RequestId" + "\\}", requestId)
				.replaceAll("\\{" + "DSAId" + "\\}", dsAId)
				.replaceAll("\\{" + "FHEModel" + "\\}", fhEModel);
		
		getSkKeyUrl = baseGetSkKeyUrl
				.replaceAll("\\{" + "RequestId" + "\\}", requestId)
				.replaceAll("\\{" + "DSAId" + "\\}", dsAId)
				.replaceAll("\\{" + "FHEModel" + "\\}", fhEModel);

		createKeyUrl = baseCreateKeyUrl
				.replaceAll("\\{" + "RequestId" + "\\}", requestId)
				.replaceAll("\\{" + "DSAId" + "\\}", dsAId)
				.replaceAll("\\{" + "FHEModel" + "\\}", fhEModel);
			
	}
	
	public boolean CreatingFHEKeyIfNeed()
	{
		String fullCheckKeyURL 	= baseUrl + checkKeyUrl;		
		String fullCreateKeyURL = baseUrl + createKeyUrl;	
		
		try {
			log.info(logStatus + "Starting request for " + fullCheckKeyURL);
			ResponseEntity<String> resCheck = m_restTemplate.getForEntity(fullCheckKeyURL, String.class);
			log.info(logStatus + " Body Response = " + resCheck.getBody());
			log.info(logStatus + "#Checking Keys getStatusCode = " + resCheck.getStatusCode());
			if(resCheck.getStatusCode() == HttpStatus.NO_CONTENT)
			{
				log.info(logStatus + "Invoking CreateFHEKey now ");
				ResponseEntity<String> resCreateKeys = m_restTemplate.getForEntity(fullCreateKeyURL, String.class);
				if(resCreateKeys.getStatusCode() != HttpStatus.CREATED)
    			{
					throw new IOException(logStatus + "Impossible to connect and Create FHE keys ");
    			}            				
			}
			else if(resCheck.getStatusCode() == HttpStatus.CREATED)
			{
				log.info(logStatus + "#Checking Keys : Is Existed !");
			}
			return true;
		} catch (HttpClientErrorException e) {
			log.info(logStatus + "HttpClientErrorException encrypt to KE-KEY-API \n " + e.toString());
		} catch (RestClientException e) {
			log.info(logStatus + "Error executing request encrypt to KE-CORE-API \n " + e.toString());			
		} catch (IOException e) {
			e.printStackTrace();
			log.info(logStatus + "Error executing request IOException to KE-CORE-API \n " + e.toString());
		}                	   
		return false;
	}
	
	public KeyTransfer getKeysForAnalysis()
	{
		String fullGetKeyURL 	= baseUrl + getKeyUrl;   
		ResponseEntity<KeyTransfer> reslstKeys = m_restTemplate.getForEntity(fullGetKeyURL, KeyTransfer.class);
		
		if(reslstKeys.getStatusCode() == HttpStatus.OK)
		{
			KeyTransfer lstKeys = reslstKeys.getBody(); 			
			return lstKeys;
		}
		return null;
	}
	
	public KeyTransfer getSkKeyForDecryption()
	{
		String fullGetKeyURL 	= baseUrl + getSkKeyUrl;   
		ResponseEntity<KeyTransfer> reslstKeys = m_restTemplate.getForEntity(fullGetKeyURL, KeyTransfer.class);
		
		if(reslstKeys.getStatusCode() == HttpStatus.OK)
		{
			KeyTransfer lstKeys = reslstKeys.getBody(); 			
			return lstKeys;
		}
		return null;
	}
	
	public byte[] getFheSecretKey(List<KeyObject> lstKeys)
	{		
		for(KeyObject keyO : lstKeys)
		{
			if(keyO.getKeyInfo().compareTo(KeyInfoEnum.FHE_SK) == 0)
				return keyO.getKey();
		}
		return null;
	}
	
	public byte[] getHEPublicKey(List<KeyObject> lstKeys)
	{
		for(KeyObject keyO : lstKeys)
		{
			if(keyO.getKeyInfo().compareTo(KeyInfoEnum.FHE_PK) == 0)
				return keyO.getKey();
		}
		return null;
	}
	
	public byte[] getHEEvaluationKey(List<KeyObject> lstKeys)
	{
		for(KeyObject keyO : lstKeys)
		{
			if(keyO.getKeyInfo().compareTo(KeyInfoEnum.FHE_EVK) == 0)
				return keyO.getKey();
		}
		return null;
	}
	
	public byte[] getHEParameter(List<KeyObject> lstKeys)
	{
		for(KeyObject keyO : lstKeys)
		{
			if(keyO.getKeyInfo().compareTo(KeyInfoEnum.FHE_PARAM) == 0)
				return keyO.getKey();
		}
		return null;
	}
	
	public boolean saveKeysInLocalStorage(KeyTransfer lstKeys, String keystoragePath) 
	{
		try {
			ToolsFolder.CreateAllFolderNames(keystoragePath);
			log.info(logStatus + "Save keys at folder " + keystoragePath);
			for(KeyObject keyO : lstKeys.getLstKeys())
			{
				if(keyO.getKeyInfo().compareTo(KeyInfoEnum.FHE_SK) == 0)
				{				
					FileOperations.PrintData(keystoragePath + File.separator + Constants.FheKeys.FHE_SK_FILE, keyO.getKey());				
				}
				if(keyO.getKeyInfo().compareTo(KeyInfoEnum.FHE_PK) == 0)
				{
					FileOperations.PrintData(keystoragePath + File.separator + Constants.FheKeys.FHE_PK_FILE, keyO.getKey());
				}
				if(keyO.getKeyInfo().compareTo(KeyInfoEnum.FHE_EVK) == 0)
				{
					FileOperations.PrintData(keystoragePath + File.separator + Constants.FheKeys.FHE_EVAL_FILE, keyO.getKey());
				}
				if(keyO.getKeyInfo().compareTo(KeyInfoEnum.FHE_PARAM) == 0)
				{
					FileOperations.PrintData(keystoragePath + File.separator + Constants.FheKeys.FHE_PARAM_FILE, keyO.getKey());
				}			
			}
		} catch (SecurityException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;		
		}
		return true;
	}
	
	public KeyTransfer loadKeysFromLocalStorage(String keyStoragePath)
	{
		KeyTransfer lstKeys = new KeyTransfer();		
		ResultEncryptedData encryptedData = ToolsFolder.getAllContentFilesInFolder(keyStoragePath);
		
		if(encryptedData == null || encryptedData.getResultAnalyse().size() == 0)
			return null;
		
		log.info(logStatus + "Get " +encryptedData.getResultAnalyse().size() +" keys at folder " + keyStoragePath);
						
		for( CipherText keyFile : encryptedData.getResultAnalyse())
		{

			if(keyFile.getFileName().equals(Constants.FheKeys.FHE_SK_FILE))
			{									
				lstKeys.addKeyObject(new KeyObject()
						.keyInfo(KeyInfoEnum.FHE_SK)
						.key(keyFile.getContent())
						);
			}
			if(keyFile.getFileName().equals(Constants.FheKeys.FHE_PK_FILE))
			{
				lstKeys.addKeyObject(new KeyObject()
						.keyInfo(KeyInfoEnum.FHE_PK)
						.key(keyFile.getContent())
						);
			}
			if(keyFile.getFileName().equals(Constants.FheKeys.FHE_EVAL_FILE))
			{
				lstKeys.addKeyObject(new KeyObject()
						.keyInfo(KeyInfoEnum.FHE_EVK)
						.key(keyFile.getContent())
						);
			}
			if(keyFile.getFileName().equals(Constants.FheKeys.FHE_PARAM_FILE))
			{
				lstKeys.addKeyObject(new KeyObject()
						.keyInfo(KeyInfoEnum.FHE_PARAM)
						.key(keyFile.getContent())
						);
			}
		}
		if(lstKeys.getLstKeys().size() == 0)
			return null;
		
		
		return lstKeys;
	}		
}
