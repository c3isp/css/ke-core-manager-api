package fr.cea.kemanager.components;

import org.springframework.web.client.RestTemplate;

public interface Component {
		
	public default RestTemplate getRestTemplate()
	{
    	return new RestTemplate();
	}
	
	String create();
	String read();
	String update();
	String delete();
	<T> T get(String urlEndPoint, String FieldName);
	
}
