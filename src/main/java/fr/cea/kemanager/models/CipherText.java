package fr.cea.kemanager.models;

public class CipherText {

	public CipherText(String fileName, byte[] content) {
		this.fileName = fileName;
		this.content = content;
	}
	
	public CipherText() {}
	
	private String fileName; 	
	private byte[] content;
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public CipherText fileName(String fileName) {
		this.fileName = fileName;
		return this;
	}
	public byte[] getContent() {
		return content;
	}
	public void setContent(byte[] content) {
		this.content = content;
	}
	public CipherText content(byte[] content) {
		this.content = content;
		return this;
	}
}
