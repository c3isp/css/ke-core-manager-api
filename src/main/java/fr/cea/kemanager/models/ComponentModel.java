package fr.cea.kemanager.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Gets or Sets ComponentModel
 */
public enum ComponentModel {
  
  DPOS("DPOS"),
  
  FHE("FHE");

  private String value;

  ComponentModel(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static ComponentModel fromValue(String text) {
    for (ComponentModel b : ComponentModel.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

