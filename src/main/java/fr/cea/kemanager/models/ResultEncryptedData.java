package fr.cea.kemanager.models;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

@Validated
public class ResultEncryptedData {
	
	
	public ResultEncryptedData() {
		super();
		RequestID = UUID.randomUUID().toString();
		ResultAnalyse = new ArrayList<CipherText>();
	}
	
	public ResultEncryptedData(String RequestID) {
		this();
		this.RequestID = RequestID; 		
	}

	public ResultEncryptedData(List<CipherText> resultAnalyse) {		
		this();
		ResultAnalyse = resultAnalyse;
	}

	/**
	 * @return the resultAnalyse
	 */
	public List<CipherText> getResultAnalyse() {
		return ResultAnalyse;
	}

	/**
	 * @param resultAnalyse the resultAnalyse to set
	 */
	public void setResultAnalyse(List<CipherText> resultAnalyse) {		
		ResultAnalyse = resultAnalyse;
	}

		public void appendChipherText(CipherText arrBytes) {
		// TODO Auto-generated method stub
		ResultAnalyse.add(arrBytes);
	} 
	
	public String getRequestID() {
		return RequestID;
	}

	public void setRequestID(String requestID) {
		RequestID = requestID;
	}

	@ApiModelProperty(example = "request ID cannot be empty", required = true, value = "")
	@NotNull
	@JsonProperty("RequestID")
	private String RequestID;
	
	@JsonProperty("ResultAnalyse")
	private List<CipherText> ResultAnalyse;

}
