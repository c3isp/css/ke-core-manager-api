package fr.cea.kemanager.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Gets or Sets TechnologyModel
 */
public enum TechnologyModel {
  
  STANDARD("Standard"),
  
  FHE("FHE");

  private String value;

  TechnologyModel(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static TechnologyModel fromValue(String text) {
    for (TechnologyModel b : TechnologyModel.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

