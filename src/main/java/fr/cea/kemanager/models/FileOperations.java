package fr.cea.kemanager.models;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.FileDeleteStrategy;
import org.apache.commons.io.FilenameUtils;

import fr.cea.kemanager.helper.ToolsFolder;


/**
 * This class encapsulates the conversions of files into byte arrays and vice-versa.
 *
 * @author Quentin Lutz
 */
public class FileOperations {
	
	public static boolean IsExisted(String filePath)
	{
		File file = new File(filePath);
		return file.exists();
	}
	/**
	 * Extract bytes from a given file.
	 *
	 * @param filePath , the path to the file
	 * @throws IOException 
	 */
	public static byte[] GetData(String filePath) throws IOException {
		Path path = Paths.get(filePath);
		return Files.readAllBytes(path);		
	}
	
	/**
	 * Print bytes into a given file
	 *
	 * @param filePath , the path to the file
	 * @param data , the data to print
	 * @throws IOException 
	 */
public static void PrintData(String filePath, byte[] data) throws IOException {
		
		Path path = Paths.get(filePath);
		File file = new File(path.toString());
		
		String parentFolder = FilenameUtils.getFullPathNoEndSeparator(file.getAbsolutePath());
		ToolsFolder.CreateAllFolderNames(parentFolder);
	    
		Files.write(path, data);		
	}
	
	/**
	 * Shred a given file.
	 *
	 * @param filePath , the path to the file
	 */
	public static void Shred(String filePath) throws IOException {
		FileDeleteStrategy.FORCE.delete(new File(filePath));
	}
	
	/**
	 * Extract bytes from a given file then shred it.
	 *
	 * @param filePath , the path to the file
	 */
	public static byte[] GetDataAndShred(String filePath) throws IOException {
		byte[] data = GetData(filePath);
		Shred(filePath);
		return data;
	}		
}
