package fr.cea.kemanager.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Gets or Sets ResponseModel
 */
public enum ResponseModel {
  
  SUCCESS("Success"),
  
  BAD_REQUEST("Bad_Request"),
  
  INTERNAL_ERROR("Internal_Error");

  private String value;

  ResponseModel(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static ResponseModel fromValue(String text) {
    for (ResponseModel b : ResponseModel.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

