/**
 *  Copyright 2017 Hewlett Packard Enterprise Development Company, L.P.
 */
package fr.cea.kemanager.test;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

/**
 * @author MIMANE
 *
 */
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
@ActiveProfiles("test") // load application-test.properties
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class KeCoreApiControllerTest {

	protected Logger log = LoggerFactory.getLogger(KeCoreApiControllerTest.class.getName());	
	
    @Autowired
    private MockMvc mockMvc;
    
    @Value("${security.user.name}")
    private String restUser;
    @Value("${security.user.password}")
    private String restPassword;

    File cifile = new File("/home/nguyen/81677462-d679-44a0-aed6-2a48d993a3f29166557881037228422/m_0.ct");
    
	@Test
    public void test01get() throws Exception {
        
       System.out.println("Testing 1");
    }
	
	@Test
    public void test02get() throws Exception {
        
        String param = "checker";
        //expected output structure = "{\"name\":\"aName\",\"id\":\"anIdValue\",\"path\":\"aPath\",\"version\":\"1\"}";
    	if(cifile.exists() == false)
    	{
    		log.info("File local test is not existed ! This test is dedicated to local test, not for production");
    		return;
    	}
    	
        String request = "/v1/fhe/decrypt/12345657/dsa/DSA-56976731-3c16-46cc-a4e1-8384c6208eb0/scheme/BLACK_LIST_FULL";
        MockMultipartFile firstFile = new MockMultipartFile("cipherTextFile", "m_0.ct", "application/octet-stream", FileUtils.readFileToByteArray(cifile));

        System.out.println("Testing 2");
//        this.mockMvc.perform(
//                post(request)
//                    .with(httpBasic(restUser, restPassword)) // basic auth
//                    .file(firstFile)
//                    .accept(MediaType.APPLICATION_JSON)
//                    .contentType(MediaType.APPLICATION_JSON)
//                 )
//                .andDo(MockMvcResultHandlers.print())
//                .andExpect(status().isAccepted());
    }
	
	@Test
    public void test03packaging() throws Exception {
        
		System.out.println("Testing 3");
        
    }
}
