<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>fr.cea.kemanager</groupId>
	<artifactId>ke-core-manager-api</artifactId>
	<version>0.1-SNAPSHOT</version>
	<packaging>war</packaging>

	<properties>
		<springfox.version>2.7.0</springfox.version>
		<swagger.version>1.5.17</swagger.version>
		<swagger2markup.version>1.3.1</swagger2markup.version>
		<kemanagerlib.version>1.0.7.RELEASE</kemanagerlib.version>
		<okhttp-version>2.7.5</okhttp-version>
	    <gson-version>2.8.1</gson-version>
	    <threetenbp-version>1.3.5</threetenbp-version>
	    <!-- <swagger-core-version>1.5.15</swagger-core-version> -->
		<!-- For CheckStyle -->
		<linkXRef>false</linkXRef>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<spring-cloud.version>Edgware.SR5</spring-cloud.version>
	</properties>

	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>1.5.8.RELEASE</version>
		<!-- <version>2.0.1.RELEASE</version> -->	
	</parent>



	<!-- to enable download from internal Nexus repositories ! -->
	<repositories>
		<repository>
        <id>nexus.snapshots</id>
        <url>http://nexusc3isp.iit.cnr.it:8081/repository/maven-snapshots/</url>
        <releases>
                <enabled>false</enabled>
        </releases>
        <snapshots>
                <enabled>true</enabled>
        </snapshots>
</repository>
<repository>
        <id>nexus.releases</id>
        <url>http://nexusc3isp.iit.cnr.it:8081/repository/maven-releases/</url>
        <releases>
                <enabled>true</enabled>
        </releases>
        <snapshots>
                <enabled>false</enabled>
        </snapshots>
</repository>
		<repository>
			<!-- Used for dependencies due to doc generation -->
			<id>jcenter-releases</id>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
			<name>jcenter</name>
			<url>http://jcenter.bintray.com</url>
		</repository>
	</repositories>

	<dependencies>
		<dependency>
			<groupId>org.springframework.cloud</groupId>
        	<artifactId>spring-cloud-starter-config</artifactId>
       	</dependency>
		<!-- spring boot dependencies plus tomcat deployed -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-tomcat</artifactId>
			<scope>provided</scope>
		</dependency>

		<!-- add log4j2 instead of default logback -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter</artifactId>
			<exclusions>
				<exclusion>
					<groupId>org.springframework.boot</groupId>
					<artifactId>spring-boot-starter-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-log4j2</artifactId>
		</dependency>

		<!-- SpringFox dependencies -->
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger2</artifactId>
			<version>${springfox.version}</version>
		</dependency>

		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger-ui</artifactId>
			<version>${springfox.version}</version>
		</dependency>

		<!-- to generate API documentation -->
		<dependency>
			<groupId>io.github.swagger2markup</groupId>
			<artifactId>swagger2markup</artifactId>
			<version>${swagger2markup.version}</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
			<version>3.7</version>
			<scope>test</scope>
		</dependency>

		<!-- swagger core dependencies -->
		<dependency>
			<groupId>io.swagger</groupId>
			<artifactId>swagger-annotations</artifactId>
			<version>${swagger.version}</version>
		</dependency>
		
		<dependency>
			<groupId>io.swagger</groupId>
			<artifactId>swagger-core</artifactId>
			<version>${swagger.version}</version>
		</dependency>

		<!-- Libraries for tests -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>

		<!-- Libraries for security -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-security</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-test</artifactId>
			<scope>test</scope>
		</dependency>

		<!-- Library for Common Event Format log; available on C3ISP Nexus Repo -->
		<dependency>
			<groupId>com.lespea</groupId>
			<artifactId>cef</artifactId>
			<version>0.0.1</version>
		</dependency>

		<dependency>
			<groupId>fr.cea.kemanager</groupId>
			<artifactId>ke-manager-lib</artifactId>
			<version>${kemanagerlib.version}</version>
			<!-- <version>1.0.2-SNAPSHOT</version> -->
		</dependency>


		<dependency>
			<groupId>com.squareup.okhttp</groupId>
			<artifactId>okhttp</artifactId>
			<version>${okhttp-version}</version>
		</dependency>
		<dependency>
			<groupId>com.squareup.okhttp</groupId>
			<artifactId>logging-interceptor</artifactId>
			<version>${okhttp-version}</version>
		</dependency>
		<dependency>
			<groupId>com.google.code.gson</groupId>
			<artifactId>gson</artifactId>
			<version>${gson-version}</version>
		</dependency>
		<dependency>
			<groupId>org.threeten</groupId>
			<artifactId>threetenbp</artifactId>
			<version>${threetenbp-version}</version>
		</dependency>
		
		<dependency>
		    <groupId>commons-io</groupId>
		    <artifactId>commons-io</artifactId>
		    <version>2.6</version>
		</dependency>

	</dependencies>


	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.springframework.cloud</groupId>
				<artifactId>spring-cloud-dependencies</artifactId>
				<version>${spring-cloud.version}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>


	<build>
		<finalName>${project.artifactId}</finalName>
		<plugins>

			<plugin>
				<artifactId>maven-compiler-plugin</artifactId>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>
			<plugin>
				<artifactId>maven-war-plugin</artifactId>
				<configuration>
					<failOnMissingWebXml>false</failOnMissingWebXml>
				</configuration>
			</plugin>

			<!-- FindBugs and FindSecurityBugs plugins -->
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>findbugs-maven-plugin</artifactId>
				<version>3.0.2</version>
				<configuration>
					<effort>Max</effort>
					<threshold>Low</threshold>
					<failOnError>true</failOnError>
					<plugins>
						<plugin>
							<groupId>com.h3xstream.findsecbugs</groupId>
							<artifactId>findsecbugs-plugin</artifactId>
							<version>1.7.1</version>
						</plugin>
					</plugins>
				</configuration>
			</plugin>

			<!-- JaCoCo plugin bound to test phase -->
			<plugin>
				<groupId>org.jacoco</groupId>
				<artifactId>jacoco-maven-plugin</artifactId>
				<version>0.7.9</version>
				<executions>
					<execution>
						<id>pre-unit-test</id>
						<goals>
							<goal>prepare-agent</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<!-- OWASP Dependency-Check plugin -->
			<plugin>
				<groupId>org.owasp</groupId>
				<artifactId>dependency-check-maven</artifactId>
				<version>3.0.2</version>
				<configuration>
					<format>XML</format>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.asciidoctor</groupId>
				<artifactId>asciidoctor-maven-plugin</artifactId>
				<version>1.5.6</version>
				<!-- Include Asciidoctor PDF for pdf generation -->
				<!-- See: https://github.com/asciidoctor/asciidoctorj/issues/416 -->
				<dependencies>
					<dependency>
						<groupId>org.asciidoctor</groupId>
						<artifactId>asciidoctorj-pdf</artifactId>
						<version>1.5.0-alpha.16</version>
					</dependency>
					<dependency>
						<groupId>org.jruby</groupId>
						<artifactId>jruby-complete</artifactId>
						<version>1.7.26</version>
					</dependency>
				</dependencies>
				<configuration>
					<!-- <sourceDirectory>${asciidoctor.input.directory}</sourceDirectory> -->
					<sourceDirectory>${project.basedir}/target/docs/asciidoc</sourceDirectory>
					<!-- <sourceDocumentName>index.adoc</sourceDocumentName> -->
					<attributes>
						<doctype>book</doctype>
						<toc>left</toc>
						<toclevels>3</toclevels>
						<numbered></numbered>
						<hardbreaks></hardbreaks>
						<sectlinks></sectlinks>
						<sectanchors></sectanchors>
						<!-- <generated>${generated.asciidoc.directory}</generated> -->
					</attributes>
				</configuration>
				<executions>
					<execution>
						<id>output-html</id>
						<phase>test</phase>
						<goals>
							<goal>process-asciidoc</goal>
						</goals>
						<configuration>
							<!-- backend values = pdf | html -->
							<backend>html</backend>
							<!-- <outputDirectory>${asciidoctor.pdf.output.directory}</outputDirectory> -->
							<sourceHighlighter>coderay</sourceHighlighter>
						</configuration>
					</execution>
				</executions>
			</plugin>

		</plugins>
	</build>

	<reporting>
		<plugins>

			<!-- CheckStyle plugin -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-checkstyle-plugin</artifactId>
				<version>2.17</version>
				<reportSets>
					<reportSet>
						<reports>
							<report>checkstyle</report>
						</reports>
					</reportSet>
				</reportSets>
			</plugin>

		</plugins>
	</reporting>

</project>
